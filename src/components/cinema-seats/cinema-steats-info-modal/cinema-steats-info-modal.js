import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Modal } from "react-bootstrap";
import { changeSeatState } from "../../../actions/seat-list.actions";
class Example extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props?.cinemaSeatData?.rowSeats?.fullName,
    };
  }
  detectFullName = (event) => {
    this.setState({
      name: event.target.value,
    });
  };
  sendData = (active) => {
    const { rowId } = this.props.cinemaSeatData;
    const { seatId } = this.props.cinemaSeatData.rowSeats;
    const data = (active, name) => {
      return {
        seatId,
        isFree: active,
        fullName: name,
        rowId,
      };
    };
    if (!active) {
      this.props.changeSeatState(data(false, ''));
      this.props.handleClose();
    } else {
      if (this.state.name) {
        alert("Спасибо ваше данные обнавлены " + this.state.name);
        this.props.changeSeatState(data(true, this.state.name));
        this.props.handleClose();
      } else {
        alert("Пожалуйста введите данные");
      }
      this.setState({
        name: '',
      });
    }
  };
  render() {
    const { show, handleClose } = this.props;
    return (
      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Body>
          {this.props?.cinemaSeatData?.rowSeats?.fullName ? (
            <div className="cinema-seats-modal-item">
              <span>
                Имя Закасчика - {this.props?.cinemaSeatData?.rowSeats?.fullName}
              </span>
            </div>
          ) : null}
          <div className="cinema-seats-modal-item">
            <span>Ряд - {this.props?.cinemaSeatData?.rowIndex + 1}</span>
          </div>
          <div className="cinema-seats-modal-item">
            <span>Сиденья - {this.props?.cinemaSeatData?.seatIndex + 1}</span>
          </div>
          <div className="cinema-seats-modal-item">
            <span>Цена - {this.props?.cinemaSeatData?.rowSeats?.price}</span>
          </div>
          {this.props?.cinemaSeatData?.rowSeats?.isFree ? null : (
            <div className="cinema-seats-modal-item">
              Заполнитье имя Закасчика{" "}
              <span>
                <input onChange={this.detectFullName} type="text" />
              </span>
            </div>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          {this.props?.cinemaSeatData?.rowSeats?.isFree ? (
            <Button
              variant="primary"
              onClick={() => {
                this.sendData(false);
              }}
            >
              Cancel the order
            </Button>
          ) : (
            <Button
              variant="primary"
              onClick={() => {
                this.sendData(true);
              }}
            >
              Buy
            </Button>
          )}
        </Modal.Footer>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  changeSeatState: (data) => dispatch(changeSeatState(data)),
});
export default connect(null, mapDispatchToProps)(Example);
