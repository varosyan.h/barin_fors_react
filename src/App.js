import React, { Component } from "react";
import {connect} from "react-redux"
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import ChinemaSteatsInfoModal from "../src/components/cinema-seats/cinema-steats-info-modal/cinema-steats-info-modal";
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      cinemaSeatData: null,
    };
  }
  handleShow = (value) => {
    this.setState({
      cinemaSeatData: value,
      show: true,
    });
  };
  handleClose=()=>{
    this.setState({
      show: false,
    });
  }
  render() {
    const {seatList} = this.props
    return (
      <div className="App monitoring-container">
        <h1>Теставое Задание</h1>
        <div>
          <ChinemaSteatsInfoModal
            handleClose={this.handleClose}
            cinemaSeatData={this.state.cinemaSeatData}
            show={this.state.show}
            
          />
          {seatList.map((row, rowIndex) => {
            return (
              <div className="cinema-seates-row" key={row.rowId}>
             
                {row.rowSeats.map((seat, seatIndex) => {
                  return (
                    <div
                      onClick={() =>
                        this.handleShow({
                          rowIndex,
                          seatIndex,
                          rowSeats: seat,
                          rowId:row.rowId
                        })
                      }
                      className={!seat.isFree?'buyc-unactive cinema-seats-container':'buyc-active cinema-seats-container'}
                      key={seat.seatId}
                    ></div>
                  );
                })}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    seatList: store.seatList.seatList
  }
};
export default connect(mapStateToProps,null)(App)

