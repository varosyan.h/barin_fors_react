const seatList = [
  {
    rowId: 1,
    rowNum: 1,
    rowSeats: [
      {
        price: 2000,
        isFree: false,
        seatId: 1,
        fullName: "",
      },
      {
        price: 1500,
        isFree: true,
        seatId: 2,
        fullName: "Gexam",

      },
      {
        price: 1500,
        isFree: false,
        seatId: 3,
        fullName: "Gexam",
      },
      {
        price: 1000,
        isFree: true,
        seatId: 4,
        fullName: "Gexam",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 5,
        fullName: "",
      },
      {
        price: 1500,
        isFree: true,
        seatId: 6,
        fullName: "Gexam",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 7,
        fullName: "",
      },
      {
        price: 1500,
        isFree: true,
        seatId: 8,
        fullName: "Gexam",
      },
    ],
  },
  {
    rowId: 2,
    rowNum: 2,
    rowSeats: [
      {
        price: 2000,
        isFree: true,
        seatId: 1,
        fullName: "Gexam",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 2,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 3,
        fullName: "",
      },
      {
        price: 1000,
        isFree: false,
        seatId: 4,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 5,
        fullName: "",
      },
      {
        price: 1500,
        isFree: true,
        seatId: 6,
        fullName: "Gexam",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 7,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 8,
        fullName: "",
      },
    ],
  },
  {
    rowId: 3,
    rowNum: 3,
    rowSeats: [
      {
        price: 2000,
        isFree: false,
        seatId: 1,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 2,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 3,
        fullName: "",
      },
      {
        price: 1000,
        isFree: true,
        seatId: 4,
        fullName: "Gexam",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 5,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 6,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 7,
        fullName: "",
      },
      {
        price: 1500,
        isFree: true,
        seatId: 8,
        fullName: "Gexam",
      },
    ],
  },
  {
    rowId: 4,
    rowNum: 4,
    rowSeats: [
      {
        price: 2000,
        isFree: false,
        seatId: 1,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 2,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 3,
        fullName: "",
      },
      {
        price: 1000,
        isFree: false,
        seatId: 4,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 5,
        fullName: "",
      },
      {
        price: 1500,
        isFree: true,
        seatId: 6,
        fullName: "Gexam",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 7,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 8,
        fullName: "",
      },
    ],
  },
  {
    rowId: 5,
    rowNum: 5,
    rowSeats: [
      {
        price: 2000,
        isFree: false,
        seatId: 1,
        fullName: "",
      },
      {
        price: 1500,
        isFree: true,
        seatId: 2,
        fullName: "Gexam",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 3,
        fullName: "",
      },
      {
        price: 1000,
        isFree: false,
        seatId: 4,
        fullName: "",
      },
      {
        price: 1500,
        isFree: true,
        seatId: 5,
        fullName: "Gexam",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 6,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 7,
        fullName: "",
      },
      {
        price: 1500,
        isFree: true,
        seatId: 8,
        fullName: "Gexam",
      },
    ],
  },
  {
    rowId: 6,
    rowNum: 6,
    rowSeats: [
      {
        price: 2000,
        isFree: false,
        seatId: 1,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 2,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 3,
        fullName: "",
      },
      {
        price: 1000,
        isFree: true,
        seatId: 4,
        fullName: "Gexam",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 5,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 6,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 7,
        fullName: "",
      },
      {
        price: 1500,
        isFree: false,
        seatId: 8,
        fullName: "",
      },
    ],
  },
];

export default seatList;
