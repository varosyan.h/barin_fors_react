import { CHANGE_SEAT_STATE } from "../constants/actions-types";
export const changeSeatState = (seatData) => {
  return {
    type: CHANGE_SEAT_STATE,
    seatData
  };
};
