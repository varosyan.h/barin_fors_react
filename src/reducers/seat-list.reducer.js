import { CHANGE_SEAT_STATE } from "../constants/actions-types";
import seatList from "../utils/seats-and-rows";
const initialState = {
  seatList,
};

function seatListReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_SEAT_STATE:
      const {seatData} = action;
      const { rowId, seatId, isFree ,fullName} = seatData;
      const rowIndex = seatList.findIndex(e => e.rowId === rowId);
      if (rowIndex > -1) {
        const row = seatList[rowIndex];
        const seatIndex = row.rowSeats.findIndex(e => e.seatId === seatId);
        if (seatIndex > -1) {
          const element =  state.seatList[rowIndex].rowSeats[seatIndex]
          element.isFree = isFree;
          element.fullName = fullName;
        } else {
          console.log("Сиденя не Найден");
        }
      } else {
        console.log("Ряд не Найден");
      }
      return { ...state};
    default:
      return state;
  }
}

export default seatListReducer;
