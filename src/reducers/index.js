import {combineReducers} from "redux";

import seatList from "./seat-list.reducer";

const rootReducer = combineReducers({
    seatList
});

export default rootReducer